
abstract class TokenProvider {
  Future<String?> getToken();
}
