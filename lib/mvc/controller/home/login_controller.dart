
import 'package:et_movie_tickets/mvc/view/home/login_page.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  RxBool isLoading = false.obs;

  Future<void> login(String email, String password) async {
    try {
      // Perform your login logic here
      // For example, you can make an API call to authenticate the user
      // Set isLoading to true before making the API call and set it back to false after the call is complete
      isLoading.value = true;

      // Simulating an API call delay
      await Future.delayed(Duration(seconds: 2));

      // After successful login, you can store the user's authentication token or other necessary data
      // You can use GetStorage or any other state management solution to store and manage data

      // Reset the form and navigate to the home page
      // You can replace `HomePage` with your own route or widget
      Get.offAll(LoginPage());
    } catch (error) {
      // Handle any errors that occur during login
      print('Login Error: $error');
    } finally {
      isLoading.value = false;
    }
  }
}